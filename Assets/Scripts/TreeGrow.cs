﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGrow : MonoBehaviour
{

    public void GrowUp(Vector2 coords)
    {

        try
        {
            if (GameObject.Find("x" + (coords.x) + "y" + (coords.y)).GetComponent<SpriteRenderer>().color == (Color.yellow))
            {
                GameObject.Find("x" + (coords.x) + "y" + (coords.y)).GetComponent<SpriteRenderer>().color = new Color(0f, 0.5f, 0f, 1f);
                Camera.main.GetComponent<Control>().queue.Enqueue(coords);
                Camera.main.GetComponent<Control>().winCap--;

            }
            else
            {
                if (GameObject.Find("x" + (coords.x) + "y" + (coords.y)).GetComponent<SpriteRenderer>().color == (Color.black))
                {

                } else
                {
                    if (GameObject.Find("x" + (coords.x) + "y" + (coords.y)).GetComponent<SpriteRenderer>().color == (Color.red))
                    {
                        Camera.main.GetComponent<Control>().winCap = 100;
                        Debug.Log("ADD THE LOSE SCREEN");
                        StartCoroutine(Lose());
                        foreach (GameObject element in GameObject.FindObjectsOfType<GameObject>())
                        {
                            if (element.GetComponent<SpriteRenderer>().color == Color.green) element.GetComponent<SpriteRenderer>().color = Color.red;
                        }
                    }
                    else
                    {
                        if (GameObject.Find("x" + (coords.x) + "y" + (coords.y)).GetComponent<SpriteRenderer>().color == (Color.blue))
                        {
                            Camera.main.GetComponent<Control>().growPoints++;
                            Camera.main.GetComponent<Control>().winCap--;
                            Camera.main.GetComponent<Control>().SeedsCount();
                            GameObject.Find("x" + (coords.x) + "y" + (coords.y)).GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 1f);
                        }
                        else
                        {
                            if (GameObject.Find("x" + (coords.x) + "y" + (coords.y)).GetComponent<SpriteRenderer>().color == (Color.white))
                            {
                                GameObject.Find("x" + (coords.x) + "y" + (coords.y)).GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 1f);
                            }
                        }
                        ;
                        //GameObject.Find("x" + (coords.x) + "y" + (coords.y)).GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 1f);
                        Debug.Log("TICK");
                    }
                }

                    
            }
        }
        catch
        {
            Debug.Log("Exep");
        }



    }

    public void OutOfGp()
    {
        StartCoroutine("Lose");
        Debug.Log("OOG");
    }


    IEnumerator Lose()
    {
        yield return new WaitForSeconds(1f);
        foreach (GameObject element in Camera.main.GetComponent<LevelGen>().prefabs)
        {
            if (element.GetComponent<SpriteRenderer>().color != Color.red)
            {
                element.GetComponent<SpriteRenderer>().color = Color.red;
                yield return new WaitForSeconds(0.01f);
            }


        }
        yield return new WaitForSeconds(1f);
        gameObject.GetComponent<LevelEditor>().LoadLevel((GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelName) + (GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb));
        for (int i = 0; i > Camera.main.GetComponent<Control>().k; i++)
        {
            Camera.main.GetComponent<Control>().coords[i] = new Vector2(0, 0);
        }

        Camera.main.GetComponent<Control>().k = 0;
        Camera.main.GetComponent<Control>().gpUsed = 0;
        Camera.main.GetComponent<Seed>().LoadSeed(Camera.main.GetComponent<Control>().curSeed);
        Camera.main.GetComponent<Control>().Starter(Camera.main.GetComponent<Control>().offset);
        Camera.main.GetComponent<Control>().Grow();

    }
}
