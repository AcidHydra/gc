﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraPos : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((SceneManager.GetActiveScene().name == "PlayField") | (SceneManager.GetActiveScene().name == "LevelField"))
        Camera.main.transform.position = new Vector3((Camera.main.GetComponent<LevelGen>().weight / 2) - 0.5f, (Camera.main.GetComponent<LevelGen>().height / 2) - 0.5f, -10);
    }
}
