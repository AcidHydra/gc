﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerateEditor : MonoBehaviour
{
    public Slider weight;
    public Slider height;


    public void gen()
    {
        Camera.main.GetComponent<LevelGen>().CustomLevel(weight.value, height.value);
    }
}
