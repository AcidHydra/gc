﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helping : MonoBehaviour
{
    public string[,] levelsWin = new string[9, 99];

    private void Start()
    {
        levelsWin[0, 1] = "1122";
        levelsWin[0, 2] = "112212";
        levelsWin[0, 3] = "22";
        levelsWin[0, 4] = "112212";
        levelsWin[0, 5] = "31321221";

        levelsWin[1, 1] = "52343243";
        levelsWin[1, 2] = "2544343213";
        levelsWin[1, 3] = "12145331";
        levelsWin[1, 4] = "3122525421";
        levelsWin[1, 5] = "344312321555";
        levelsWin[1, 6] = "22433332122431";

        levelsWin[2, 1] = "7455425256";
        levelsWin[2, 2] = "77445547";
        levelsWin[2, 3] = "4344655341211326";
        levelsWin[2, 4] = "65434555227566";
        levelsWin[2, 5] = "1624463552";
        levelsWin[2, 6] = "53356512527256";
        levelsWin[2, 7] = "325442411125";

        Debug.Log("CurGP " + Camera.main.GetComponent<Control>().growPoints);
    }

    public void HelpRequest()
    {
        /*
        Camera.main.GetComponent<Control>().k = 0;
        Camera.main.GetComponent<Control>().gpUsed = 0;
        Camera.main.GetComponent<Seed>().LoadSeed(Camera.main.GetComponent<Control>().curSeed);/*
        Camera.main.GetComponent<Control>().Starter(Camera.main.GetComponent<Control>().offset);
        Camera.main.GetComponent<Control>().Grow();*/

        StartCoroutine(PrepRestart());

        


    }

    IEnumerator PrepRestart()
    {
        yield return new WaitForSecondsRealtime(0.1f);
        int requestCount = Camera.main.GetComponent<Control>().requestCount;

        int curLev = Camera.main.GetComponent<Control>().curLevel;
        char[] winPath = levelsWin[curLev / 10, curLev % 10].ToCharArray(0, levelsWin[curLev / 10, curLev % 10].Length);
        Debug.Log("Help starting " + curLev + " " + curLev / 10 + " " + curLev % 10);

        for (int i = 0; i < requestCount; i++)
        {
            try
            {
                Debug.Log("Help starting " + (int.Parse(winPath[i * 2].ToString()), int.Parse(winPath[(i * 2) + 1].ToString())));
                Camera.main.GetComponent<Click>().ClickSim(int.Parse(winPath[i * 2].ToString()) - 1, (int.Parse(winPath[(i * 2) + 1].ToString())) - 1);
            }
            catch { }

        }
        Camera.main.GetComponent<Control>().requestCount++;
    }

}
