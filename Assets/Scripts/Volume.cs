﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Volume : MonoBehaviour
{
    public AudioMixerGroup Mixer;
    public Toggle _toggle;

    public void ToggleMusic(bool enabled)
    {
        if (enabled)
        {
            Mixer.audioMixer.SetFloat("Volume", 0);
            Camera.main.GetComponent<Control>().boop = true;
            Camera.main.GetComponent<LevelSeeds>().LoadSavedData();
            Camera.main.GetComponent<LevelSeeds>().saveFile.music = true;
            Camera.main.GetComponent<LevelSeeds>().SaveData();
        } else
        {
            Mixer.audioMixer.SetFloat("Volume", -80);
            Camera.main.GetComponent<Control>().boop = false;
            Camera.main.GetComponent<LevelSeeds>().LoadSavedData();
            Camera.main.GetComponent<LevelSeeds>().saveFile.music = false;
            Camera.main.GetComponent<LevelSeeds>().SaveData();
        }



    }

    public void ChangeVolume(float arg)
    {
        Mixer.audioMixer.SetFloat("Master", Mathf.Lerp(-80, 0, arg/100));
        Camera.main.GetComponent<LevelSeeds>().LoadSavedData();
        Camera.main.GetComponent<LevelSeeds>().saveFile.volume = arg;
        Camera.main.GetComponent<LevelSeeds>().SaveData();

    }
    public void ButtonChange()
    {
        if (_toggle.isOn == true)
        {
            Mixer.audioMixer.SetFloat("Volume", -80);
            Camera.main.GetComponent<Control>().boop = false;
            _toggle.isOn = false;
            Camera.main.GetComponent<LevelSeeds>().LoadSavedData();
            Camera.main.GetComponent<LevelSeeds>().saveFile.music = false;
            Camera.main.GetComponent<LevelSeeds>().SaveData();
        } else
        {
            Mixer.audioMixer.SetFloat("Volume", 0);
            Camera.main.GetComponent<Control>().boop = true;
            _toggle.isOn = true;
            Camera.main.GetComponent<LevelSeeds>().LoadSavedData();
            Camera.main.GetComponent<LevelSeeds>().saveFile.music = true;
            Camera.main.GetComponent<LevelSeeds>().SaveData();
        }
    }

    
}
