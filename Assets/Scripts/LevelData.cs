﻿using UnityEngine;

[System.Serializable]


public class LevelData
{
    public int growPoints;
    public int winCap;

    public int weight;
    public int height;

    public float[,] yellow = new float[99, 99];
    public float[,] black = new float[99, 99];
    public float[,] blue = new float[99, 99];
    public float[,] red = new float[99, 99];

    public LevelData (int growPointsInt, int winCapInt, int weightInt, int heightInt, float[,] yell, float[,] blac, float[,] blu, float[,] re)
    {
        growPoints = growPointsInt;
        winCap = winCapInt;

        weight = weightInt;
        height = heightInt;

        yellow = yell;
        black = blac;
        blue = blu;
        red = re;
    }

}