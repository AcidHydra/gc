using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class RewardedAds : MonoBehaviour, IUnityAdsListener
{
    [SerializeField] private bool _testMode = true;
    [SerializeField] private Button _adsButton;

    private string _gameId = "4280193"; //��� game id

    private string _rewardedVideo = "Rewarded_Android";

    void Start()
    {
        _adsButton = GetComponent<Button>();
        _adsButton.interactable = Advertisement.IsReady(_rewardedVideo);


        Debug.Log("Ads button init");
        Advertisement.AddListener(this);
        Advertisement.Initialize(_gameId, true);
    }

    private void Update()
    {
        if (_adsButton.interactable == true)
        {
            _adsButton.transform.Find("Image").GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f, 1);
        }
        else
        {
            _adsButton.transform.Find("Image").GetComponent<Image>().color = new Color(1, 1, 1, 1);
        }

        
    }

    public void ShowRewardedVideo()
    {

        Advertisement.Show(_rewardedVideo);
    }

    public void OnUnityAdsReady(string placementId)
    {
        if (placementId == _rewardedVideo)
        {
            _adsButton.interactable = true; //��������, ���� ������� ��������
        }
    }

    private void OnDestroy()
    {
        Advertisement.RemoveListener(this);
    }

    public void OnUnityAdsDidError(string message)
    {
        //������ �������
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // ������ ��������� �������
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult) //��������� ������� (��� ����������� ��������������)
    {
        if (showResult == ShowResult.Finished)
        {
            if (placementId == "Rewarded_Android")
                gameObject.GetComponent<Helping>().HelpRequest();
            
            //��������, ���� ������������ ��������� ������� �� �����
        }
        else if (showResult == ShowResult.Skipped)
        {
            //��������, ���� ������������ ��������� �������
        }
        else if (showResult == ShowResult.Failed)
        {
            //�������� ��� ������
        }
    }
}
