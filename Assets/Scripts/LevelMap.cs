﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMap : MonoBehaviour
{
    public Texture2D levelMap;
    // Start is called before the first frame update
    void Start()
    {
        Texture2D texture = new Texture2D(3, 3);
        GetComponent<Renderer>().material.mainTexture = texture;

        for (int y = 0; y < texture.height; y++)
        {
            for (int x = 0; x < texture.width; x++)
            {
                Color color = ((x & y) != 0 ? Color.white : Color.gray);
                texture.SetPixel(x, y, color);
            }
        }
        texture.Apply();
        gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("LevelMapBlank");
        
        
        
    }

    // Update is called once per frame
    void Update()
    {
       // levelMap.SetPixel(2, 2, Color.white);
       // levelMap.Apply();
        Debug.Log("Pixel 1/1 " + levelMap.GetPixel(2, 2));
        
    }
}
