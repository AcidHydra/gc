﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickEditor : MonoBehaviour
{
    public int state;

    private void Update()
    {
        state = Camera.main.GetComponent<Control>().curState;
    }

    private void OnMouseDown()
    {
        if (Camera.main.GetComponent<LevelEditor>().edit == true)
        {
            switch (state)
            {
                case 1:
                    GetComponent<SpriteRenderer>().color = Color.yellow;
                    break;
                case 2:
                    GetComponent<SpriteRenderer>().color = Color.blue;
                    break;
                case 3:
                    GetComponent<SpriteRenderer>().color = Color.black;
                    break;
                case 4:
                    GetComponent<SpriteRenderer>().color = Color.red;
                    break;
                case 5:
                    GetComponent<SpriteRenderer>().color = Color.white;
                    break;
                default:
                    break;
            }
        }
            
    }
}
