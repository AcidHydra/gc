﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//using GoogleMobileAds.Api;
using UnityEngine.Audio;

public class Control : MonoBehaviour
{
    public Vector2[] coords = new Vector2[10];

    public Vector2 offset;
    public int growPoints;
    public int gpUsed;
    public int k = 0;
    public int curState;
    public int winCap;
    
    public InputField console;
    public string curSeed;
    public int curLevel;
    public int gameSpeed = 1;
    public Vector3 camStartPos;

    public bool secondClick;
    public bool busy;
    public bool boop = true;
    public char prevScene;
    public float dpsTime;
    public int requestCount = 1;


    public AudioMixerGroup mixer;



    public Queue<Vector2> queue = new Queue<Vector2>();

    

    private void Awake()
    {
        

        DontDestroyOnLoad(gameObject);
        //MobileAds.Initialize(InitializationStatus => { });
        prevScene = 'q';
    }

    // Start is called before the first frame update
    void Start()
    {
        


        camStartPos = Camera.main.transform.position;
        gameObject.GetComponent<LevelSeeds>().LoadSavedData();

        if (gameObject.GetComponent<LevelSeeds>().saveFile.music) mixer.audioMixer.SetFloat("Volume", 0);
        else mixer.audioMixer.SetFloat("Volume", -80);

        mixer.audioMixer.SetFloat("Master", Mathf.Lerp(-80, 0, gameObject.GetComponent<LevelSeeds>().saveFile.volume / 100));
    }

    // Update is called once per frame
    void Update()
    {
        
        dpsTime =((float)AudioSettings.dspTime%0.5f);
        //Debug.Log(dpsTime);

        if (Input.GetKey(KeyCode.Escape)) Camera.main.GetComponent<UIButtons>().Back();
        if (Input.GetKeyDown("z")) curState = 1;
        if (Input.GetKeyDown("x")) curState = 2;
        if (Input.GetKeyDown("c")) curState = 3;
        if (Input.GetKeyDown("v")) curState = 4;
        if (Input.GetKeyDown("b")) curState = 5;
        if (Input.GetKeyDown("`"))
        {
            if (console.IsActive())
            {
                console.gameObject.SetActive(false);
            }
            else
            {
                console.gameObject.SetActive(true);
            }    

        }

        
        
        


       
        Debug.Log("WinCap" + winCap);

        try
        {
            GameObject.Find("EditorText").GetComponent<Text>().text = " " + curState;
            Debug.Log(". . .");
        }
        catch
        {
            Debug.Log("No editor");
        }

    }

    public void save(float x, float y)
    {
        coords[k] = new Vector2(x, y);
        offset = coords[0];
        k++;
             
    }

    public void Grow()
    {
        StartCoroutine(Grow2());

    }

    public void Starter(Vector2 v2)
    {
        queue.Enqueue(v2);
    }

   

    

    IEnumerator Win()
    {
        yield return new WaitForSeconds(1f);
        Debug.Log("cur lvl " + curLevel);
        if (curLevel == 5) NewData(11);
        if (curLevel == 16) NewData(21);
        NewData(curLevel + 1);

        foreach (GameObject element in Camera.main.GetComponent<LevelGen>().prefabs)
        {
            if (element.GetComponent<SpriteRenderer>().color != Color.green)
            {
                element.GetComponent<SpriteRenderer>().color = Color.green;
                yield return new WaitForSeconds(0.01f);
            }

            
        }
        yield return new WaitForSeconds(1f);
        /*
        Debug.Log("Level name " + GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelName);

        if ((GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelName == "tutor") & (GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb == 3))
        {
            GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb = 1;
            GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelName = "level";
            gameObject.GetComponent<LevelEditor>().LoadLevel("level" + (GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb));
        }
        else
        {
            if (GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelName.Contains("tutor"))
            {
                GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb++;
                gameObject.GetComponent<LevelEditor>().LoadLevel("tutor" + (GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb));
            }
            else
            {
                GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb++;
                gameObject.GetComponent<LevelEditor>().LoadLevel("level" + (GameObject.Find("PlayManager").GetComponent<PlayManager>().curLevelNumb));
            }
            
            
        }*/
        for (int i = 0; i > k; i++)
        {
            coords[i] = new Vector2(0, 0);
        }

        k = 0;
        Debug.Log("ln " + ((curLevel / 10), ((curLevel % 10) + 1)));
        Camera.main.GetComponent<LevelSeeds>().GoToLevel((curLevel / 10), ((curLevel % 10) + 1));


        curLevel++;
        gpUsed = 0;
        //SceneManager.LoadScene("Levels", LoadSceneMode.Single);
    }

    IEnumerator Grow2()
    {
        

        for (int j = 0; j < queue.Count;)
        {
            for (int i = 0; i < k; i++)
            {
                yield return new WaitForSeconds(gameSpeed * 0.03f);
                Camera.main.GetComponent<TreeGrow>().GrowUp(coords[i] + queue.Peek() - offset);
            }
            queue.Dequeue();
        }
        busy = false;
        SeedsCount();
        check();
        //check();
    }


    public void check()
    {
        if ((growPoints <= 0) & (winCap > 0))
        {
            gameObject.GetComponent<TreeGrow>().OutOfGp();
            Debug.Log("lose wc= " + winCap);
        }
        else
        {
            if (winCap <= 0 & busy == false & growPoints <= 0)
            {
                StartCoroutine(Win());
                Debug.Log("win win");
            }
        }
    }

    
    
    public void SeedsCount()
    {
        try
        {
            
            GameObject.Find("s0").GetComponent<Image>().enabled = false;
            GameObject.Find("s1").GetComponent<Image>().enabled = false;
            GameObject.Find("s2").GetComponent<Image>().enabled = false;
            GameObject.Find("s3").GetComponent<Image>().enabled = false;


            GameObject.Find("s" + growPoints.ToString()).GetComponent<Image>().enabled = true; 
        }
        catch
        {
            Debug.Log("Seeds count " + growPoints);
        }
        

    }
    
    
    void NewData(int arg1)
    {
        Camera.main.GetComponent<LevelSeeds>().LoadSavedData();

        Camera.main.GetComponent<LevelSeeds>().saveFile.unlocks[arg1] = true;

        Camera.main.GetComponent<LevelSeeds>().saveFile.progress[arg1-1] = true;

        Camera.main.GetComponent<LevelSeeds>().SaveData();
    }

    

}
