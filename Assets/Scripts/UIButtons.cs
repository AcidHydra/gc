﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIButtons : MonoBehaviour
{
    public void Restore()
    {
        for (int i = 1; i < 99; i++)
        {
            Camera.main.GetComponent<LevelSeeds>().saveFile.unlocks[i] = false;
            Camera.main.GetComponent<LevelSeeds>().saveFile.progress[i] = false;
        }



        


        Camera.main.GetComponent<LevelSeeds>().SaveData();
        Destroy(Camera.main.gameObject);
        SceneManager.LoadScene("PreLoad");

    }




    public void GoToMenu()
    {
        
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
        Camera.main.GetComponent<Control>().gpUsed = 0;
        Camera.main.GetComponent<Control>().k = 0;

    }

    public void EditorButton()
    {
        SceneManager.LoadScene("LevelField", LoadSceneMode.Single);
        Camera.main.GetComponent<Control>().prevScene = 'm';
    }

    public void PlayButton()
    {
        SceneManager.LoadScene("Levels", LoadSceneMode.Single);
        Camera.main.GetComponent<Control>().prevScene = 'm';
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Undo()
    {
        if ((Camera.main.GetComponent<Control>().k > 0) & (Camera.main.GetComponent<Control>().busy == false))
        {
            Camera.main.GetComponent<Control>().k--;
            Camera.main.GetComponent<Control>().gpUsed--;
            Camera.main.GetComponent<Seed>().LoadSeed(Camera.main.GetComponent<Control>().curSeed);
            Camera.main.GetComponent<Control>().Starter(Camera.main.GetComponent<Control>().offset);
            Camera.main.GetComponent<Control>().Grow();
            Camera.main.GetComponent<Control>().check();
            
            
        }

        if (Camera.main.GetComponent<Control>().k == 0)
        {
            Destroy(GameObject.Find("SeedPref"));
        }
        
    }
    public void TestLevel()
    {
        Camera.main.GetComponent<Control>().prevScene = 't';
        Camera.main.GetComponent<LevelEditor>().edit = false;
        Camera.main.GetComponent<Control>().curState = 0;
        Camera.main.GetComponent<Seed>().GenSeed();
        Camera.main.GetComponent<Control>().curSeed = Camera.main.GetComponent<Seed>().seed;
        SceneManager.LoadScene("PlayField", LoadSceneMode.Single);
    }

    public void Restart()
    {
        Camera.main.GetComponent<Control>().k = 0;
        Camera.main.GetComponent<Control>().gpUsed = 0;
        Camera.main.GetComponent<Seed>().LoadSeed(Camera.main.GetComponent<Control>().curSeed);
        Camera.main.GetComponent<Control>().Starter(Camera.main.GetComponent<Control>().offset);
        Camera.main.GetComponent<Control>().Grow();
    }

    public void Back()
    {
        Camera.main.GetComponent<Control>().k = 0;
        if (Camera.main.GetComponent<Control>().prevScene == 'l')
        {
            SceneManager.LoadScene("Levels", LoadSceneMode.Single);
            Camera.main.GetComponent<Control>().gpUsed = 0;
            Camera.main.GetComponent<Control>().prevScene = 'm';
        }
        else
        {
            if (Camera.main.GetComponent<Control>().prevScene == 't')
            {
                SceneManager.LoadScene("LevelField", LoadSceneMode.Single);
                Camera.main.GetComponent<LevelEditor>().edit = true;
                Camera.main.GetComponent<Control>().prevScene = 'e';
            }
            else
            {
                if (Camera.main.GetComponent<Control>().prevScene == 'e' || Camera.main.GetComponent<Control>().prevScene == 'm')
                {
                    SceneManager.LoadScene("Menu", LoadSceneMode.Single);
                    Camera.main.GetComponent<Control>().prevScene = 'q';
                    Camera.main.GetComponent<Control>().curSeed = null;
                }
                else
                {
                    if (Camera.main.GetComponent<Control>().prevScene == 'q') Application.Quit();
                }
            }

        }
    }
}
