﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{
    public GameObject seeds;

    public void Start()
    {
        Camera.main.GetComponent<LevelSeeds>().LoadSavedData();
        Camera.main.GetComponent<LevelSeeds>().saveFile.unlocks[1] = true;
        string[] numb = gameObject.name.Split('.');
        
        
        if ((Camera.main.GetComponent<LevelSeeds>().saveFile.unlocks[((int.Parse(numb[0]) * 10) + (int.Parse(numb[1])))]) == false)
        {
            GetComponent<Button>().image.color = new Color(0.5f, 0.5f, 0.5f, 1);
            GetComponent<Button>().enabled = false;
        }

        if ((Camera.main.GetComponent<LevelSeeds>().saveFile.progress[((int.Parse(numb[0]) * 10) + (int.Parse(numb[1])))]) == true)
        {
            GetComponent<Button>().image.color = new Color(0f, 1f, 0f, 1);
        }

        //Debug.Log("level numb = " + ((int.Parse(numb[0]) * 10) + (int.Parse(numb[1]))) + Camera.main.GetComponent<LevelSeeds>().saveFile.progress[((int.Parse(numb[0]) * 10) + (int.Parse(numb[1])))]);
        //try { Debug.Log("level numb = " + ((int.Parse(numb[0]) * 100) + (int.Parse(numb[1]) * 10) + (int.Parse(numb[2])))); } catch { Debug.Log("level numb = " + ((int.Parse(numb[0]) * 10) + (int.Parse(numb[1])))); }
    }

    

    public void Onclick()
    {
        string[] numb = gameObject.name.Split('.');
        Camera.main.GetComponent<Control>().curLevel = ((int.Parse(numb[0])*10) + (int.Parse(numb[1])));
        seeds.GetComponent<LevelSeeds>().GoToLevel((int.Parse(numb[0])), (int.Parse(numb[1])));
        Camera.main.GetComponent<Control>().prevScene = 'l';
    }

}
