﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Seed : MonoBehaviour
{
    public GameObject tile;
    public string seed;

    private void Start()
    {
        if (Camera.main.GetComponent<Control>().prevScene == 'e')
        {
            GameObject.Find("Seed").GetComponent<Seed>().LoadSeed(Camera.main.GetComponent<Control>().curSeed);
        }
    }

    public void GenSeed()
    {
        seed = "";
        float weight = Camera.main.GetComponent<LevelGen>().weight;
        float height = Camera.main.GetComponent<LevelGen>().height;
        

        Adding(weight);
        Adding(height);
        Adding(9);
        Adding(9);

        Debug.Log(seed);
        
        for (float x = 0f; x < weight; x++)
        {
            for (float y = 0f; y < height; y++)
            {
                tile = GameObject.Find("x" + (int)x + "y" + (int)y);
                Adding(GetColor(tile.GetComponent<SpriteRenderer>().color));
            }
        }
        
        Debug.Log(seed);
        
    }
    private void Adding(object arg1)
    {
        seed = (seed + "" + arg1);
    }

    private string GetColor(Color arg1)
    {
        if (arg1 == Color.yellow)
        {
            return "1";
        }
        else
            if (arg1 == Color.blue)
        {
            return "2";
        }
        else
            if (arg1 == Color.red)
        {
            return "3";
        }
        else
            if (arg1 == Color.black)
        {
            return "4";
        }
        else
            return "5";

    }
    public void LoadSeed(string arg1) //5533555555555
    {
        try
        {
            char[] arr = arg1.ToCharArray();
            Camera.main.GetComponent<LevelGen>().weight = int.Parse(arr[0].ToString());
            Camera.main.GetComponent<LevelGen>().height = int.Parse(arr[1].ToString());
            Camera.main.GetComponent<LevelGen>().growPoints = int.Parse(arr[2].ToString()) - Camera.main.GetComponent<Control>().gpUsed;
            Camera.main.GetComponent<Control>().growPoints = int.Parse(arr[2].ToString()) - Camera.main.GetComponent<Control>().gpUsed;
            Camera.main.GetComponent<LevelGen>().winCap = int.Parse(arr[3].ToString());
            Camera.main.GetComponent<LevelGen>().seed = arg1;
            Camera.main.GetComponent<LevelGen>().LoadLevel();
        }
        catch
        {
            SceneManager.LoadScene("Levels", LoadSceneMode.Single);
        }
        


        /*
        int i = 0;
        Debug.Log("recoloting");
        for (float x = 0f; x < int.Parse(arr[0].ToString()); x++)
        {
            for (float y = 0f; y < int.Parse(arr[1].ToString()); y++)
            {
                tile = GameObject.Find("x" + (int)x + "y" + (int)y);
                switch (int.Parse(arr[4 + i].ToString()))
                {
                    case 1:
                        tile.GetComponent<SpriteRenderer>().color = Color.yellow;
                        Debug.Log("yellow coloring");
                        break;
                    case 2:
                        tile.GetComponent<SpriteRenderer>().color = Color.blue;
                        break;
                    case 3:
                        tile.GetComponent<SpriteRenderer>().color = Color.red;
                        break;
                    case 4:
                        tile.GetComponent<SpriteRenderer>().color = Color.black;
                        break;
                    case 5:
                        tile.GetComponent<SpriteRenderer>().color = Color.white;
                        break;
                }
                i++;

            }
        }*/
    }
}
