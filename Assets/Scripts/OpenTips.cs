﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenTips : MonoBehaviour
{
    public GameObject g1;
    public GameObject g2;
    
    public void Open()
    {
        g1.SetActive(false);
        g2.SetActive(true);
        Camera.main.GetComponent<Control>().busy = true;
        

    }
    public void Close()
    {
        g1.SetActive(true);
        g2.SetActive(false);
        Camera.main.GetComponent<Control>().busy = false;
    }
}
