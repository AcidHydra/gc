﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
[System.Serializable]

public class LevelSeeds : MonoBehaviour
{
    public string[,] levelsNumber = new string[9,99];

    private void Start()
    {
        levelsNumber[0, 1] = "3321554515455";
        levelsNumber[0, 2] = "3323554411452";
        levelsNumber[0, 3] = "3311553414533";
        levelsNumber[0, 4] = "3322554515432";
        levelsNumber[0, 5] = "3333412553515";

        levelsNumber[1, 1] = "55324542554435314554555455454";
        levelsNumber[1, 2] = "55334413455555555152555543244";
        levelsNumber[1, 3] = "55333555354545155555444455142";
        levelsNumber[1, 4] = "55323555551545544455524231553";
        levelsNumber[1, 5] = "55333554525354355542513432545";
        levelsNumber[1, 6] = "55334555435253511525255345524";

        levelsNumber[2, 1] = "77335555444542535455244555143343555411554553545555444";
        levelsNumber[2, 2] = "77335354353323413553543555555445444415545455354444555";
        levelsNumber[2, 3] = "77393552553515551535555535551255315155355525153552523";
        levelsNumber[2, 4] = "77274255555212554445532455514545452515555535554555555";
        levelsNumber[2, 5] = "77334455454534154542345545455415514534555542344555454";
        levelsNumber[2, 6] = "77332555552443534455511554444355515551544351342555552";
        levelsNumber[2, 7] = "77335354444255455435544545144444554155555255555555525";

        Debug.Log("CurGP " + Camera.main.GetComponent<Control>().growPoints);
    }


    public class SaveProgress
    {
        public bool[] progress = new bool[1024];
        public bool[] unlocks = new bool[1024];
        public bool music;
        public float volume;
    }
    public SaveProgress saveFile = new SaveProgress();

    public void LoadSavedData()
    {
        if (PlayerPrefs.HasKey("Save"))
        {
            string saveJson = PlayerPrefs.GetString("Save");
            saveFile = JsonUtility.FromJson<SaveProgress>(saveJson);
        }
        else
        {
            saveFile.unlocks[1] = true;
            SaveData();
        }
    }
    public void SaveData()
    {
        string saveJson = JsonUtility.ToJson(saveFile);
        PlayerPrefs.SetString("Save", saveJson);
        PlayerPrefs.Save();
    }
    

    



    public void GoToLevel(int arg1, int arg2)
    {
        Debug.Log("arg1 " + arg1);
        Debug.Log("arg2 " + arg2);
        Debug.Log("seed " + levelsNumber[0, 1]);
        Camera.main.GetComponent<Control>().curSeed = levelsNumber[arg1, arg2];
        SceneManager.LoadScene("PlayField", LoadSceneMode.Single);
        
    }
}
