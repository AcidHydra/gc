﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
[System.Serializable]

public class SaveLoad : MonoBehaviour
{

    public string level;

    public int curGP;
    public int curWC;
    public int curWe;
    public int curHe;
    public float[,] curYe = new float[99, 99];
    public float[,] curBla = new float[99, 99];
    public float[,] curBlu = new float[99, 99];
    public float[,] curRe = new float[99, 99];



    public void SaveLevel()
    {
        if (Directory.Exists(Application.dataPath + "/levels"))
        {

        } else
        {
            Directory.CreateDirectory(Application.dataPath + "/levels");
        }

        string path = (Application.dataPath + "/levels/" + level + ".dat"); //"D:/levels/" Application.dataPath + "/levels/"

        LevelData curlevel = new LevelData(curGP, curWC, curWe, curHe, curYe, curBla, curBlu, curRe);

        

        Stream filestream = File.Create(path);
        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(filestream, curlevel);
        filestream.Close();

    }

    public void LoadLevel()
    {

        string path = (Application.dataPath + "/levels/" + level + ".dat");

        if (File.Exists(path))
        {
            Debug.Log("Loading");

            Stream filestream = File.OpenRead(path);
            BinaryFormatter bf = new BinaryFormatter();
            LevelData curLevel = (LevelData)bf.Deserialize(filestream);
            filestream.Close();

            


            Camera.main.GetComponent<LevelGen>().growPoints = curLevel.growPoints;
            Camera.main.GetComponent<Control>().growPoints = curLevel.growPoints;
            Camera.main.GetComponent<LevelGen>().winCap = curLevel.winCap;
            Camera.main.GetComponent<LevelGen>().weight = curLevel.weight;
            Camera.main.GetComponent<LevelGen>().height = curLevel.height;



            for (int i = 0; i < (curLevel.weight * curLevel.height); i++)
            {
                Camera.main.GetComponent<LevelGen>().yellow[i] = new Vector2(curLevel.yellow[i, 0], curLevel.yellow[i, 1]);
            }

            for (int i = 0; i < (curLevel.weight * curLevel.height); i++)
            {
                Camera.main.GetComponent<LevelGen>().black[i] = new Vector2(curLevel.black[i, 0], curLevel.black[i, 1]);
            }

            for (int i = 0; i < (curLevel.weight * curLevel.height); i++)
            {
                Camera.main.GetComponent<LevelGen>().blue[i] = new Vector2(curLevel.blue[i, 0], curLevel.blue[i, 1]);
            }

            for (int i = 0; i < (curLevel.weight * curLevel.height); i++)
            {
                Camera.main.GetComponent<LevelGen>().red[i] = new Vector2(curLevel.red[i, 0], curLevel.red[i, 1]);
            }

            Camera.main.GetComponent<LevelGen>().LoadLevel();

        }
        else
        {
            Debug.LogError("File not found");
            return;
        }

        

        
    }
}
