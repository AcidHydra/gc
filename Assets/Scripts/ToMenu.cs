﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class ToMenu : MonoBehaviour
{
    public AudioMixerGroup mixer;
    // Start is called before the first frame update
    void Start()
    {



        gameObject.GetComponent<LevelSeeds>().LoadSavedData();

        if (gameObject.GetComponent<LevelSeeds>().saveFile.music) mixer.audioMixer.SetFloat("Volume", 0);
        else mixer.audioMixer.SetFloat("Volume", -80);

        mixer.audioMixer.SetFloat("Master", Mathf.Lerp(-80, 0, gameObject.GetComponent<LevelSeeds>().saveFile.volume / 100));
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }

    // Update is called once per frame
    void Update()
    {
        
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
        gameObject.GetComponent<LevelSeeds>().LoadSavedData();

        if (gameObject.GetComponent<LevelSeeds>().saveFile.music) mixer.audioMixer.SetFloat("Volume", 0);
        else mixer.audioMixer.SetFloat("Volume", -80);

        mixer.audioMixer.SetFloat("Master", Mathf.Lerp(-80, 0, gameObject.GetComponent<LevelSeeds>().saveFile.volume / 100));
    }
}
