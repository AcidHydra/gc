﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Click : MonoBehaviour
{

    public Grid grid;
    public Vector3Int cell;
    




    private void OnMouseEnter()
    {
        if (Camera.main.GetComponent<LevelEditor>().edit == false)
        {
            /*
            if ((GetComponent<SpriteRenderer>().color == new Color(1f, 1f, 1f, 1f)))
            {
                GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f, 1);
            } */
        }
        else
        {

            

        }


    }
    private void OnMouseExit()
    {

        if ((GetComponent<SpriteRenderer>().color == new Color(1f, 1f, 1f, 1f)))
        {
            

        } else
        {
            if (GetComponent<SpriteRenderer>().color == new Color(0.5f, 0.5f, 0.5f, 1))
            {
                GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            }
        }




    }

    

    private void OnMouseDown()
    {
        if (Camera.main.GetComponent<Control>().busy == false)

        if (Camera.main.GetComponent<LevelEditor>().edit == false)
        {
            

                if (Camera.main.GetComponent<Control>().growPoints > 0)
                {

                    if (GetComponent<SpriteRenderer>().color == new Color(1f, 1f, 1f, 1f))
                    {
                            Camera.main.GetComponent<Control>().growPoints--;
                            Camera.main.GetComponent<Control>().gpUsed++;
                            GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 1f);
                            
                            string[] data = gameObject.name.Trim('x').Split('y');
                            /*if (Camera.main.GetComponent<Control>().k == 0)
                            {
                                Instantiate(Camera.main.GetComponent<LevelGen>().tile, new Vector3((float.Parse(data[0])), (float.Parse(data[1])), -1), Quaternion.identity);
                                GameObject.Find("Square Pref(Clone)").GetComponent<SpriteRenderer>().color = new Color(0f, 0.7f, 0f, 1f);
                                GameObject.Find("Square Pref(Clone)").name = ("SeedPref");
                            } */
                            Camera.main.GetComponent<Control>().save((float.Parse(data[0])), (float.Parse(data[1])));
                            Camera.main.GetComponent<Control>().SeedsCount();
                            if (Camera.main.GetComponent<Control>().secondClick == true)
                            {
                                Camera.main.GetComponent<Control>().busy = true;
                                Debug.Log("restart level");
                                    //Camera.main.GetComponent<Seed>().LoadSeed("77395555555515555555155555521555555555555554555555455");
                                Camera.main.GetComponent<Seed>().LoadSeed(Camera.main.GetComponent<Control>().curSeed);
                                Camera.main.GetComponent<Control>().Starter(Camera.main.GetComponent<Control>().offset);
                                Camera.main.GetComponent<Control>().Grow();
                            
                            }
                            
                    }
                    else
                    {

                        if (GetComponent<SpriteRenderer>().color == (Color.yellow))
                        {
                            Camera.main.GetComponent<Control>().busy = true;

                            Camera.main.GetComponent<Control>().winCap--;
                            //Camera.main.GetComponent<Control>().growPoints = 0;
                            string[] data = gameObject.name.Trim('x').Split('y');

                            GetComponent<SpriteRenderer>().color = new Color(0f, 0.5f, 0f, 1f);

                            Camera.main.GetComponent<Control>().save((float.Parse(data[0])), (float.Parse(data[1])));
                            Camera.main.GetComponent<Control>().Starter(new Vector2((float.Parse(data[0])), (float.Parse(data[1]))));
                            Camera.main.GetComponent<Control>().secondClick = true;
                            Camera.main.GetComponent<Control>().Grow();
                            Camera.main.GetComponent<Control>().growPoints--;
                            Camera.main.GetComponent<Control>().gpUsed++;
                            Camera.main.GetComponent<Control>().SeedsCount();
                            

                        }
                    }
                }
            
        }

        //if (secondClick == true) Camera.main.GetComponent<Control>().Grow();

        //GameObject.Find("x" + 3 + "y" + 3).GetComponent<Tree>().GrowUp(2, 2);
        
    }

    public void ClickSim(int x, int y)
    {
        Debug.Log("Get " + x + y);
        GameObject obj = GameObject.Find("x" + x + "y" + y);

        if (obj.GetComponent<SpriteRenderer>().color == new Color(1f, 1f, 1f, 1f))
        {
            Camera.main.GetComponent<Control>().growPoints--;
            Camera.main.GetComponent<Control>().gpUsed++;
            obj.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, 0f, 1f);

            string[] data = obj.gameObject.name.Trim('x').Split('y');
            
            Camera.main.GetComponent<Control>().save((float.Parse(data[0])), (float.Parse(data[1])));
            Camera.main.GetComponent<Control>().SeedsCount();
            if (Camera.main.GetComponent<Control>().secondClick == true)
            {
                Camera.main.GetComponent<Control>().busy = true;
                Debug.Log("restart level");
                //Camera.main.GetComponent<Seed>().LoadSeed("77395555555515555555155555521555555555555554555555455");
                Camera.main.GetComponent<Seed>().LoadSeed(Camera.main.GetComponent<Control>().curSeed);
                Camera.main.GetComponent<Control>().Starter(Camera.main.GetComponent<Control>().offset);
                Camera.main.GetComponent<Control>().Grow();

            }

        }
        else
        {

            if (obj.GetComponent<SpriteRenderer>().color == (Color.yellow))
            {
                Camera.main.GetComponent<Control>().busy = true;

                Camera.main.GetComponent<Control>().winCap--;
                //Camera.main.GetComponent<Control>().growPoints = 0;
                string[] data = obj.gameObject.name.Trim('x').Split('y');

                obj.GetComponent<SpriteRenderer>().color = new Color(0f, 0.5f, 0f, 1f);

                Camera.main.GetComponent<Control>().save((float.Parse(data[0])), (float.Parse(data[1])));
                Camera.main.GetComponent<Control>().Starter(new Vector2((float.Parse(data[0])), (float.Parse(data[1]))));
                Camera.main.GetComponent<Control>().secondClick = true;
                Camera.main.GetComponent<Control>().Grow();
                Camera.main.GetComponent<Control>().growPoints--;
                Camera.main.GetComponent<Control>().gpUsed++;
                Camera.main.GetComponent<Control>().SeedsCount();


            }
        }


    }
    
}
