﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetVerif : MonoBehaviour
{
    public GameObject rG;
    public GameObject sG;

    public void SwitchSure()
    {
        rG.SetActive(false);
        sG.SetActive(true);
    }
    public void SwitchReset()
    {
        rG.SetActive(true);
        sG.SetActive(false);
    }
}
