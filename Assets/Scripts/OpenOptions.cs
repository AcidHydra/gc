﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenOptions : MonoBehaviour
{
    public GameObject g1;
    public GameObject g2;
    public Slider _slider;
    public Toggle _toggle;
    public void Open()
    {
        g1.SetActive(false);
        g2.SetActive(true);
        Camera.main.GetComponent<Control>().busy = true;
        Camera.main.GetComponent<LevelSeeds>().LoadSavedData();
        if (Camera.main.GetComponent<LevelSeeds>().saveFile.music == false) _toggle.isOn = false;
        _slider.value = Camera.main.GetComponent<LevelSeeds>().saveFile.volume;

    }
    public void Close()
    {
        g1.SetActive(true);
        g2.SetActive(false);
        Camera.main.GetComponent<Control>().busy = false;
    }
}
